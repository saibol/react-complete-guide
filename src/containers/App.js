import React,{Component} from 'react';
import './App.css';
import Blog from '../components/Blog/Blog';
import {BrowserRouter} from 'react-router-dom';


class App extends Component {
  
  render(){
    return (
      <BrowserRouter>
      <div className="App">
        <h4>I'm React</h4>
        <Blog></Blog>
      </div>
      </BrowserRouter>
    );
  }
}
export default App;
