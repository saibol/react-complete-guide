import React from 'react';
import './PostLoader.css';

const PostLoader = () => {
    return <div className="loader"></div>
}
export default PostLoader;