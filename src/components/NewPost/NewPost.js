import React, { Component } from 'react';
import './NewPost.css';

class NewPost extends Component {

    state = {
        title: '',
        body: '',
        author: 'sunil'
    }

    newPostHandler = () => {
        console.log('inside newPostHandler', this.state.title, this.state.body, this.state.author);
        const post = {
            title: this.state.title, 
            body: this.state.body, 
            author: this.state.author
        }  
        this.props.newPostHandler(post);
    }

    render() {
        return (
            <div className="new-post">
                <div className="row">
                    <label>title</label>
                    <input
                        type="text"
                        value={this.state.title}
                        onChange={event => { this.setState({ title: event.target.value }) }}
                    ></input>
                </div>
                <div className="row">
                    <label >description</label>
                    <input type="text"
                        value={this.state.body}
                        onChange={event => { this.setState({ body: event.target.value }) }}
                    ></input>
                </div>
                <div className="row">
                    <label >author</label>
                    <select
                        value={this.state.author}
                        onChange={event => { this.setState({ author: event.target.value }) }} >
                        <option value="sunil">sunil</option>
                        <option value="andrea">andrea</option>
                    </select>
                </div>
                <button 
                onClick={this.newPostHandler}
                >Add Post</button>
            </div>
        );
    }
}
export default NewPost;