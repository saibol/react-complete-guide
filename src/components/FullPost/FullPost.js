import React, {Component}from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import './FullPost.css';


class FullPost extends Component {

    state = {
        loadedPost: null
    }
    componentDidMount() {
        if (this.props.match.params.id) {
            axios.get('https://jsonplaceholder.typicode.com/posts/' + this.props.match.params.id)
                .then(res => {
                    this.setState({
                        loadedPost: res.data
                    })
                })
                .catch(err =>
                    console.log('error occurred while loading post', err)
                )
        }
    }

    render() {
        const post = this.state.loadedPost ? (<div className="full-post">
            <p>{this.state.loadedPost.title}</p>
            <p>{this.state.loadedPost.body}</p>
        </div>) : (
                <p>Please select the Post</p>);

        return (
            <div>
                {post}
            </div>);
    }
}
export default withRouter(FullPost);