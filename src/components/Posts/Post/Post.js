import React from 'react';
import './Post.css';
import { withRouter } from 'react-router-dom';

const Post = props => {
    console.log(props);
    return (
    <div className="post" onClick={props.postSelected}>
        <p>{props.title}</p>
        <p>{props.author}</p>
    </div>);
};

export default withRouter(Post);