import React, { Component } from 'react';
import Post from './Post/Post';
import axios from 'axios';
import './Posts.css';
import { Link } from 'react-router-dom';
import PostLoader from '../../UI/PostLoader/PostLoader';

class Posts extends Component {

    state = {
        posts: [], 
        hasPostLoaded: false
    }

    newPostHandler = post => {
        const posts = [...this.state.posts];
        posts.push(post);
        this.setState({ posts: posts })
    }

    componentDidMount() {
        
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(res => {
                this.setState({
                    posts: res.data.splice(0, 4),
                    hasPostLoaded: true
                })
            }).catch(err =>
                console.log('error occurred while fetching posts')
            )
    }

    render() {
        const author = 'sunil';
        let posts = [...this.state.posts];

        let result = <PostLoader/>;
        if(this.state.hasPostLoaded){
            result = posts.map(post => {
                return <Link to={'/'+post.id} key={post.id} >
                            <Post
                                title={post.title}
                                author={author} />
                        </Link>
                    }
            );
        }

        return (
            <div className="posts">
                {result}
            </div>
        );
    }
}
export default Posts;