import React, { Component } from 'react';
import './Blog.css';
import Posts from '../Posts/Posts';
import FullPost from '../FullPost/FullPost';
import NewPost from '../NewPost/NewPost';
import { Route, NavLink, Switch } from 'react-router-dom';

class Blog extends Component {

    render() {
        return (
            <div className="blog">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink to="/" exact>Home</NavLink></li>
                            <li><NavLink to="/0" >Full Post</NavLink></li>
                            <li><NavLink to="/new-post">New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                    <Route path="/" exact component={Posts}></Route>
                    <Route path="/new-post" exact component={NewPost}></Route>
                    <Route path="/:id" exact component={FullPost}></Route>
                </Switch>
            </div>);
    }
}
export default Blog;